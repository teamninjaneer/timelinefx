package heidtmare.timelinefx;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Duration;

public class TimelineSample extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Platform.setImplicitExit(true);

        Timeline timeline = new Timeline(Instant.now(), Duration.hours(1), Duration.hours(12));
        timeline.setPrefSize(1000, 200);
        StackPane root = new StackPane(timeline);

        stage.setTitle("Timeline Sample");
        stage.setScene(new Scene(root));
        stage.show();

        Instant now = Instant.now();
        timeline.getTimelineItems().addAll(
                new TimelineItem(now, "Now", "desc"),
                new TimelineItem(now.minus(1, ChronoUnit.HOURS), "Now minus an hour", "desc"),
                new TimelineItem(now.minus(12, ChronoUnit.HOURS), "Now minus 12 hours", "desc"),
                new TimelineItem(now.minus(1, ChronoUnit.DAYS), "Now minus a day", "desc"),
                new TimelineItem(now.plus(1, ChronoUnit.HOURS), "Now plus an hour", "desc"),
                new TimelineItem(now.plus(12, ChronoUnit.HOURS), "Now plus 12 hours", "desc"),
                new TimelineItem(now.plus(1, ChronoUnit.DAYS), "Now plus a day", "desc")
        );

        new AnimationTimer() {
            @Override
            public void handle(long now) {
                timeline.setDisplayDate(timeline.getDisplayDate().plusSeconds(1));
            }
        }.start();
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application. main() serves only as
     * fallback in case the application can not be launched through deployment artifacts, e.g., in
     * IDEs with limited FX support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
