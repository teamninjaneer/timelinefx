/**
 * Copyright © 2014, Terramenta. All rights reserved.
 *
 * This work is subject to the terms of either
 * the GNU General Public License Version 3 ("GPL") or
 * the Common Development and Distribution License("CDDL") (collectively, the "License").
 * You may not use this work except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://opensource.org/licenses/CDDL-1.0
 * http://opensource.org/licenses/GPL-3.0
 */
package heidtmare.timelinefx;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;
import java.util.stream.Collectors;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.geometry.Side;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.chart.NumberAxis;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.util.Duration;
import javafx.util.StringConverter;

/**
 *
 * @author Chris Heidt <chris.heidt@vencore.com>
 */
public class Timeline extends Region {

    private static final int TICK_COUNT = 20;
    private final NumberAxis axis;
    private final Polygon displayDurationRegion;
    private final Line displayDateLine;
    private final Group timelineItemMarkers = new Group();
    private final ObservableList<TimelineItem> timelineItems = FXCollections.<TimelineItem>observableArrayList();
    private final ObservableList<TimelineItem> sortedTimelineItems = new SortedList<>(timelineItems, (TimelineItem a, TimelineItem b) -> {
        return a.getDateTime().compareTo(b.getDateTime());
    });
    private Instant displayDate;
    private Duration displayDuration;
    private Duration boundingDuration;
    private double priorX;
    private double startingY;

    public Timeline() {
        this(Instant.now(), Duration.hours(1), Duration.hours(12));
    }

    public Timeline(Instant displayDate, Duration displayDuration, Duration boundingDuration) {
        this.displayDate = displayDate;
        this.displayDuration = displayDuration;
        this.boundingDuration = boundingDuration;

        //auto scaling number axis
        axis = new NumberAxis();
        axis.setAutoRanging(false);//!important
        axis.setSide(Side.TOP);
        axis.prefWidthProperty().bind(this.widthProperty());
        axis.prefHeightProperty().bind(this.heightProperty());
        axis.setAnimated(false);//!important
        axis.setMinorTickVisible(true);
        axis.setTickLabelRotation(90);//vertical labels
        axis.setTickLabelFormatter(new StringConverter<Number>() {
            private final DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
                    .withLocale(Locale.US)
                    .withZone(ZoneId.systemDefault());

            @Override
            public String toString(Number seconds) {
                return formatter.format(Instant.ofEpochSecond(seconds.longValue()));
            }

            @Override
            public Number fromString(String label) {
                return formatter.parse(label, Instant::from).getEpochSecond();
            }

        });

        //display data line
        displayDateLine = new Line();
        displayDateLine.setStrokeWidth(2);
        displayDateLine.setStroke(Color.GRAY);
        displayDateLine.startYProperty().bind(axis.layoutYProperty());
        displayDateLine.endYProperty().bind(Bindings.add(axis.layoutYProperty(), axis.heightProperty()));

        //display duration region
        displayDurationRegion = new Polygon();
        displayDurationRegion.setOpacity(0.4);
        displayDurationRegion.setStrokeWidth(1);
        displayDurationRegion.setStroke(Color.GRAY);
        displayDurationRegion.setFill(Color.LIGHTGRAY);

        //set all kids back to front
        this.getChildren().addAll(displayDateLine, axis, displayDurationRegion, timelineItemMarkers);

        //listen for data changes
        sortedTimelineItems.addListener((Observable obs) -> {
            timelineItemMarkers.getChildren().setAll(
                    sortedTimelineItems.stream()
                            .map(ti -> {
                                double x = axis.getDisplayPosition(ti.getDateTime().getEpochSecond());
                                Line marker = new Line();
                                marker.setStartX(x);
                                marker.setEndX(x);
                                marker.setStartY(axis.getLayoutY());
                                marker.setEndY(axis.getLayoutY() + axis.getHeight());
                                marker.setUserData(ti);
                                marker.setStroke(Color.MAGENTA);
                                marker.setStrokeWidth(1);
                                return marker;
                            })
                            .collect(Collectors.toList())
            );
        });

        //listen for layout changes. Will trigger when displayed
        axis.widthProperty().addListener((Observable obs) -> {
            update();
        });
        axis.heightProperty().addListener((Observable obs) -> {
            update();
        });

        //scroll handler for bounding duration adjusting
        this.addEventHandler(ScrollEvent.SCROLL, e -> {
            adjustBoundingDuration(e.getDeltaY());
        });

        //mouse handlers for display duration adjusting
        this.addEventHandler(MouseEvent.MOUSE_PRESSED, e -> {
            priorX = e.getSceneX();

            //set drag styling
            this.setCursor(Cursor.H_RESIZE);
            displayDateLine.setStroke(Color.DARKKHAKI);
            displayDurationRegion.getStrokeDashArray().addAll(10d, 5d);
            displayDurationRegion.setStroke(Color.DARKKHAKI);
            displayDurationRegion.setFill(Color.KHAKI);
        });
        this.addEventHandler(MouseEvent.MOUSE_DRAGGED, e -> {
            double offsetX = e.getSceneX() - priorX;
            priorX = e.getSceneX();
            adjustDisplayDuration(offsetX);
        });
        this.addEventHandler(MouseEvent.MOUSE_RELEASED, e -> {
            priorX = Double.NaN;

            //reset styling
            this.setCursor(Cursor.DEFAULT);
            displayDateLine.setStroke(Color.GRAY);
            displayDurationRegion.getStrokeDashArray().clear();
            displayDurationRegion.setStroke(Color.GRAY);
            displayDurationRegion.setFill(Color.LIGHTGRAY);
        });
    }

    public Instant getDisplayDate() {
        return displayDate;
    }

    public void setDisplayDate(Instant selectedInstant) {
        this.displayDate = selectedInstant;
        update();
    }

    public Duration getBoundingDuration() {
        return boundingDuration;
    }

    public void setBoundingDuration(Duration duration) {
        if (duration != null && duration.lessThan(Duration.ONE)) {
            //restrict to positive bounds
            this.boundingDuration = Duration.seconds(1);
        } else {
            //note: null is valid
            this.boundingDuration = duration;
        }

        //if bounds are less than display set, display to bounds
        Duration dd = getDisplayDuration();
        if (dd != null && duration != null && duration.lessThan(dd)) {
            setDisplayDuration(duration);
        }

        update();
    }

    public Duration getDisplayDuration() {
        return displayDuration;
    }

    public void setDisplayDuration(final Duration duration) {
        if (duration != null && duration.lessThan(Duration.ONE)) {
            //restrict to positive bounds
            this.displayDuration = Duration.seconds(1);
        } else if (duration != null && duration.greaterThan(getBoundingDuration())) {
            //restrict to  bounding duration
            this.displayDuration = getBoundingDuration();
        } else {
            //note: null is valid
            this.displayDuration = duration;
        }

        update();
    }

    public ObservableList<TimelineItem> getTimelineItems() {
        return timelineItems;
    }

    //adjust bounding duration
    private void adjustBoundingDuration(double offset) {
        Duration sd = getBoundingDuration();
        if (sd == null) {
            return;
        }

        double zeroPixels = axis.getDisplayPosition(0d);
        double deltaPixels = zeroPixels + offset;
        double deltaSeconds = axis.getValueForDisplay(deltaPixels).doubleValue();
        Duration newDuration = sd.add(Duration.seconds(deltaSeconds));
        setBoundingDuration(newDuration);
    }

    //adjust selected duration
    private void adjustDisplayDuration(double offset) {
        Duration dd = getDisplayDuration();
        if (dd == null) {
            return;
        }

        double zeroPixels = axis.getDisplayPosition(0d);
        double deltaPixels = zeroPixels + offset;
        double deltaSeconds = axis.getValueForDisplay(deltaPixels).doubleValue();
        Duration newDuration = dd.add(Duration.seconds(deltaSeconds));
        setDisplayDuration(newDuration);
    }

    private void update() {
        displayDateLine.setVisible(false);
        displayDurationRegion.setVisible(false);

        //cant do much without these values
        if (displayDate == null || boundingDuration == null) {
            return;
        }

        long displayDateInSeconds = displayDate.getEpochSecond();
        updateAxis(displayDateInSeconds);
        updateDisplayIndicators(displayDateInSeconds);
        updateTimelineItemMarkers(displayDateInSeconds);
    }

    private void updateAxis(long displayDateInSeconds) {
        //set axis bounds
        double boundingDurationInSeconds = boundingDuration.toSeconds();
        double halfBoundingDurationInSeconds = boundingDurationInSeconds / 2;
        double tickSpacingInSeconds = boundingDurationInSeconds / TICK_COUNT;
        axis.setLowerBound(displayDateInSeconds - halfBoundingDurationInSeconds);
        axis.setUpperBound(displayDateInSeconds + halfBoundingDurationInSeconds);
        axis.setTickUnit(tickSpacingInSeconds);
    }

    private void updateDisplayIndicators(long displayDateInSeconds) {
        //draw display date line
        double nowScreenX = axis.getDisplayPosition(displayDateInSeconds);
        displayDateLine.setStartX(nowScreenX);
        displayDateLine.setEndX(nowScreenX);
        displayDateLine.setVisible(true);

        if (displayDuration == null) {
            return;
        }

        //draw display duration region
        double halfDisplayDurationInSeconds = displayDuration.toSeconds() / 2;
        double minScreenX = axis.getDisplayPosition(displayDateInSeconds - halfDisplayDurationInSeconds);
        double maxScreenX = axis.getDisplayPosition(displayDateInSeconds + halfDisplayDurationInSeconds);
        displayDurationRegion.getPoints().setAll(minScreenX, 0d, maxScreenX, 0d, maxScreenX, axis.getHeight(), minScreenX, axis.getHeight());
        displayDurationRegion.setVisible(true);
    }

    private void updateTimelineItemMarkers(long displayDateInSeconds) {
        if (timelineItemMarkers.getChildren().isEmpty()) {
            return;
        }

        double boundingDurationInSeconds = boundingDuration.toSeconds();
        double halfBoundingDurationInSeconds = boundingDurationInSeconds / 2;
        double lowerBoundInSeconds = displayDateInSeconds - halfBoundingDurationInSeconds;
        double upperBoundInSeconds = displayDateInSeconds + halfBoundingDurationInSeconds;

        //update position of markers
        timelineItemMarkers.getChildren().forEach(tim -> {
            Line marker = (Line) tim;
            marker.setVisible(false);
            long sec = ((TimelineItem) marker.getUserData()).getDateTime().getEpochSecond();
            if (sec >= lowerBoundInSeconds && sec <= upperBoundInSeconds) {
                double x = axis.getDisplayPosition(sec);
                marker.setStartX(x);
                marker.setEndX(x);
                marker.setStartY(axis.getLayoutY());
                marker.setEndY(axis.getLayoutY() + axis.getHeight());
                marker.setVisible(true);
            }
        });

    }
}
